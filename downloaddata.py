import requests
from bs4 import BeautifulSoup

r = requests.get('https://www.mcdonalds.com.my/storefinder/index.php', allow_redirects = True)

soup = BeautifulSoup(r.content, 'html.parser')
results = soup.find(id = 'results')
print(results.prettify())
